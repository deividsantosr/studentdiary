<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Request;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Token;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    private $jwt;

    /**
     * Set default properties children controllers
     * Controller constructor.
     */
    public function __construct()
    {
        $this->jwt = Request::bearerToken();
    }

    /**
     * To use in other methods
     * @return Token
     */
    public function getToken()
    {
        $parser = new Parser();

        $token = $parser->parse($this->jwt);

        return $token;
    }

    /**
     * Info about user logged
     * @return User
     */
    public function getLogin()
    {
        return User::where(array(
            'hash' => $this->getToken()->getClaim('uid')
        ))->first();
    }
}
