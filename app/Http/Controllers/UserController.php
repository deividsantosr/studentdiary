<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    private $slug;

    /**
     * Set URL params in properties
     * UserController constructor.
     */
    public function __construct()
    {
        $this->slug = Input::get('slug');
    }

    /**
     * Action to get points of a user
     * @return Response
     */
    public function point()
    {
        $content = array(
            'success' => false
        );

        $v = Validator::make(
            array(
                'slug' => $this->slug
            ),
            array(
                'slug' => 'required|exists:user,slug'
            )
        );

        if (!$v->fails()) {
            $user = User::where(array(
                'slug' => $this->slug,
                'status' => 1
            ))->first();

            if ($user) {
                $content['point'] = $this->getPoint($user->id);

                $content['success'] = true;
            } else {
                $content['message'] = 'Invalid user';
            }
        } else {
            $content['message'] = $v->messages();
        }

        return response($content, Response::HTTP_OK);
    }

    /**
     * Total of points based in each value theme rating of the user(Fibonacci Sequence)
     * @param $id_user
     * @return int
     */
    private function getPoint($id_user)
    {
        $total = 0;
        $total += User::getTotalRating($id_user, 1) * $this->getPointKey(1);
        $total += User::getTotalRating($id_user, 2) * $this->getPointKey(2);
        $total += User::getTotalRating($id_user, 3) * $this->getPointKey(3);
        $total += User::getTotalRating($id_user, 4) * $this->getPointKey(4);
        $total += User::getTotalRating($id_user, 5) * $this->getPointKey(5);

        return $total;
    }

    /**
     * Points by rating
     * @param $num
     * @return int
     */
    private function getPointKey($num)
    {
        $value = Config::get('point.value_' . $num);

        return intval($value);
    }
}