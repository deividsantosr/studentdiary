<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Tag;
use App\Theme;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ThemeController extends Controller
{
    private $perPage;

    private $catId;

    private $userId;

    private $title;

    private $description;

    private $content;

    private $tag;

    /**
     * Set URL params in properties
     * ThemeController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->perPage = Input::get('per_page');
        $this->catId = Input::get('cat_id');
        $this->userId = $this->getLogin()->id;
        $this->title = Input::get('title');
        $this->description = Input::get('description');
        $this->content = Input::get('content');
        $this->tag = Input::get('tag');
    }

    /**
     * Action to get theme feed
     * @return Response
     */
    public function index()
    {
        $content = array(
            'success' => false
        );

        $feed = Theme::getGlobalFeed()->paginate($this->perPage);

        if ($feed->count()) {
            $content['feed'] = $feed;
            $content['success'] = true;
        }

        return response($content, Response::HTTP_OK);
    }


    /**
     * Action to create a theme
     * @return Response
     */
    public function create()
    {
        $content = array(
            'success' => false
        );

        $v = Validator::make(
            array(
                'cat_id' => $this->catId,
                'title' => $this->title,
                'description' => $this->description,
                'content' => $this->content
            ),
            array(
                'cat_id' => 'required|numeric|exists:category,id',
                'title' => 'required|string|max:200',
                'description' => 'required|string|max:300',
                'content' => 'required|string'
            )
        );

        if (!$v->fails()) {
            $theme = new Theme();

            $theme->cat_id = $this->catId;
            $theme->user_id = $this->userId;
            $theme->title = $this->title;
            $theme->description = $this->description;
            $theme->content = $this->content;
            $theme->status = 1;

            $theme->save();

            $this->createTag($theme->id);

            $content['success'] = true;
        } else {
            $content['message'] = $v->messages();
        }

        return response($content, Response::HTTP_OK);
    }

    /**
     * Create tag to theme
     * @param $theme_id
     * @return void
     */
    public function createTag($theme_id)
    {
        //Turn tag param in array
        $items = explode(',', $this->tag);


        foreach ($items as $item) {
            $v = Validator::make(
                array(
                    'tag' => $item,
                    'theme_id' => $theme_id
                ),
                array(
                    'tag' => 'required|string|max:200',
                    'theme_id' => 'required|numeric|exists:theme,id'
                )
            );

            //Save only if not fails
            if (!$v->fails()) {
                $tag = new Tag();

                $tag->theme_id = $theme_id;
                $tag->name = $item;

                $tag->save();
            }
        }
    }
}
