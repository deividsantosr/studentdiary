<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Rating;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class RatingController extends Controller
{
    private $userFromId;

    private $themeId;

    private $value;

    /**
     * Set URL params in properties
     * RatingController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->userFromId = $this->getLogin()->id;
        $this->themeId = Input::get('theme_id');
        $this->value = Input::get('value');
    }

    /**
     * Action to create a rating
     * @return Response
     */
    public function create()
    {
        $content = array(
            'success' => false
        );

        if (!$this->getValidation()->fails()) {
            $hasRating = Rating::where(array(
                'user_from_id' => $this->userFromId,
                'theme_id' => $this->themeId
            ))->first();

            if (!$hasRating) {
                $content['success'] = true;

                $rating = new Rating();

                $rating->user_from_id = $this->userFromId;
                $rating->theme_id = $this->themeId;
                $rating->value = $this->value;

                $rating->save();
            } else {
                $content['message']['error'] = 'You have already rated';
            }
        } else {
            $content['message'] = $this->getValidation()->messages();
        }

        return response($content, Response::HTTP_OK);

    }

    /**
     * Action to update a rating
     * @return Response
     */
    public function update()
    {
        $content = array(
            'success' => false
        );

        if (!$this->getValidation()->fails()) {
            $rating = Rating::where(array(
                'user_from_id' => $this->userFromId,
                'theme_id' => $this->themeId
            ))->first();

            if ($rating) {
                $content['success'] = true;

                $rating->value = $this->value;

                $rating->save();
            } else {
                $content['message']['error'] = 'Inexistent rating';
            }
        } else {
            $content['message'] = $this->getValidation()->messages();
        }

        return response($content, Response::HTTP_OK);
    }

    /**
     * Action to delete a rating
     * @return Response
     */
    public function remove()
    {
        $content = array(
            'success' => false
        );

        $v = Validator::make(
            array(
                'user_id' => $this->userFromId,
                'theme_id' => $this->themeId
            ),
            array(
                'user_id' => 'required|exists:user,id',
                'theme_id' => 'required|exists:theme,id'
            )
        );

        if (!$v->fails()) {
            $rating = Rating::where(array(
                'user_from_id' => $this->userFromId,
                'theme_id' => $this->themeId
            ));

            if ($rating->delete()) {
                $content['success'] = true;
            } else {
                $content['message']['error'] = 'Error delete rating';
            }
        } else {
            $content['message'] = $v->messages();
        }

        return response($content, Response::HTTP_OK);
    }

    /**
     * Validation parameters
     * @return Validator
     */
    private function getValidation()
    {
        $v = Validator::make(
            array(
                'user_id' => $this->userFromId,
                'theme_id' => $this->themeId,
                'value' => $this->value
            ),
            array(
                'user_id' => 'required|exists:user,id',
                'theme_id' => 'required|exists:theme,id',
                'value' => 'required|numeric|min:1|max:5'
            )
        );

        return $v;
    }
}
