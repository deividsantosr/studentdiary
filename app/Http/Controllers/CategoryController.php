<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller
{
    /**
     * Action to get categories
     * @return Response
     */
    public function index()
    {
        $content = array(
            'success' => false
        );

        $category = Category::getSimpleList();

        if ($category->count()) {
            $content['data'] = $category;
            $content['success'] = true;
        }

        return response($content, Response::HTTP_OK);
    }
}
