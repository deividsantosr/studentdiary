<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class AccountController extends Controller
{
    private $email;
    private $pwd;
    private $firstName;
    private $lastName;
    private $birthDate;

    /**
     * Set URL params in properties
     * AccountController constructor.
     */
    public function __construct()
    {
        $this->email = Input::get('email');
        $this->pwd = Input::get('pwd');
        $this->firstName = Input::get('first_name');
        $this->lastName = Input::get('last_name');
        $this->birthDate = Input::get('birth_date');
    }

    /**
     * Action to create a new account
     * @return Response
     */
    public function create()
    {
        $content = array(
            'success' => false
        );

        $v = Validator::make(
            array(
                'email' => $this->email,
                'pwd' => $this->pwd,
                'first_name' => $this->firstName,
                'last_name' => $this->lastName,
                'birth_date' => $this->birthDate
            ),
            array(
                'email' => 'required|email|unique:user',
                'pwd' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'birth_date' => 'required|date'
            )
        );


        if (!$v->fails()) {
            $user = new User();

            $user->email = $this->email;
            $user->pwd = $this->generatePwdHash($this->pwd);
            $user->hash = $this->generateHash($this->email);
            $user->first_name = $this->firstName;
            $user->last_name = $this->lastName;
            $user->slug = $this->generateSlug($this->firstName . $this->lastName);
            $user->status = true;
            $user->birth_date = $this->birthDate;

            $user->save();

            $content['success'] = true;
        } else {
            $content['message'] = $v->messages();
        }

        return response($content);
    }

    /**
     * Action to login
     * @return Response
     */
    public function login()
    {
        $content = array(
            'success' => false
        );

        $response = new Response();

        $v = Validator::make(
            array(
                'email' => $this->email,
                'pwd' => $this->pwd
            ),
            array(
                'email' => 'required|email',
                'pwd' => 'required'
            )
        );

        if (!$v->fails()) {

            $login = User::where(array(
                'email' => $this->email,
                'pwd' => $this->generatePwdHash($this->pwd)
            ))->first();

            if ($login) {
                $content['success'] = true;

                $response->header('Authorization', 'Bearer ' . $this->generateToken($login->hash));
                $response->setContent($content);
                $response->setStatusCode($response::HTTP_OK);

                return $response;
            } else {
                $content['message']['auth'] = 'Invalid credentials';
            }
        } else {
            $content['message'] = $v->messages();
        };

        $response->setContent($content);
        $response->setStatusCode($response::HTTP_UNAUTHORIZED);

        return $response;
    }

    /**
     * Hash generator based on password input
     * @param $pwd
     * @return string
     */
    private function generatePwdHash($pwd)
    {
        return hash('sha256', $pwd);
    }

    /**
     * Random token for user selection on API
     * @param $email
     * @return string
     */
    private function generateHash($email)
    {
        return bcrypt($email);
    }

    /**
     * JWT generator
     * @param $id
     * @return string
     */
    private function generateToken($id)
    {
        $config = Config::get('jwt');

        if ($config['exp']) {
            $config['exp'] = time() + $config['exp'];
        }

        $builder = new Builder();
        $signer = new Sha256();

        /**
         * ISS = Request from domain
         * AUD = Request to domain
         * EXP = Expiration time
         * JTI = Token id, can use in black-list
         * UID = Hash to verify login
         * KEY = Encrypt
         */
        $token = $builder->setIssuer($config['iss'])
                         ->setAudience($config['aud'])
                         ->setExpiration($config['exp'])
                         ->setId(bcrypt($id), true)
                         ->set('uid', $id)
                         ->sign($signer, $config['key'])
                         ->getToken();

        return (string)$token;
    }

    /**
     * Generate URL to access user profile
     * @param $str
     * @return string
     */
    private function generateSlug($str)
    {
        $slug = str_slug($str);

        $v = Validator::make(
            array('slug' => $slug),
            array('slug' => 'required|unique:user')
        );

        //Increment rand number on slug
        if ($v->fails()) {
            $slug = $this->generateSlug($slug . rand());
        }

        return $slug;
    }
}
