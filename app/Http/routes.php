<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'api'], function () {
    //Account
    Route::group(['prefix' => 'account'], function () {
        Route::post('create', 'AccountController@create');

        Route::post('login', 'AccountController@login');
    });

    //Required Auth
    Route::group(['middleware' => 'jwt.auth'], function () {
        //User
        Route::group(['prefix' => 'user'], function () {
            Route::get('point', 'UserController@point');
        });

        //Category
        Route::group(['prefix' => 'category'], function () {
            Route::get('/', 'CategoryController@index');
        });

        //Theme
        Route::group(['prefix' => 'theme'], function () {
            Route::get('/', 'ThemeController@index');

            Route::post('create', 'ThemeController@create');
        });

        //Rating
        Route::group(['prefix' => 'rating'], function () {
            Route::post('create', 'RatingController@create');

            Route::put('update', 'RatingController@update');

            Route::delete('remove', 'RatingController@remove');
        });
    });
});

Route::get('/', function () {
    return view('welcome');
});
