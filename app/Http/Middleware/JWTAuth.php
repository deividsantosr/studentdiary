<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Config;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response;

class JWTAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $jwt = $request->bearerToken();

        //Block unauthorized request
        if (!$this->check($jwt)) {
            return response(array(
                'success' => false,
                'message' => 'Unauthorized'
            ), Response::HTTP_UNAUTHORIZED);
        }

        return $next($request);
    }

    /**
     * Check user authorization on API
     * @param $jwt
     * @return bool
     */
    public function check($jwt)
    {
        $auth = false;

        try {
            $parser = new Parser();

            $signer = new Sha256();

            $token = $parser->parse($jwt);

            $hash = $token->getClaim('uid');

            //Validate by encrypt, login and expiration time
            if ($token->verify($signer, $this->getKey()) && $this->canLogin($hash) && !($token->getClaim('exp') && $token->isExpired())) {
                $auth = true;
            }
        } catch (\Exception $e) {
            //Invalid token
        }

        return $auth;
    }

    /**
     * @param $hash
     * @return User
     */
    public function canLogin($hash)
    {
        return User::where(array(
            'hash' => $hash,
            'status' => true
        ))->first();
    }

    /**
     * Application key
     * @return string
     */
    public function getKey()
    {
        return (string)Config::get('jwt.key');
    }
}
