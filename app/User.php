<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'user';

    protected $hidden = array(
        'id',
        'pwd',
        'hash'
    );

    /**
     * Total rating, based on user and value
     * @param $id_user
     * @param $value_rating
     * @return int
     */
    public static function getTotalRating($id_user, $value_rating)
    {
        $builder = self::from('rating')
                       ->join('theme', 'theme.id', '=', 'rating.theme_id')
                       ->where('theme.user_id', $id_user)
                       ->where('rating.value', $value_rating);

        return $builder->count();
    }
}
