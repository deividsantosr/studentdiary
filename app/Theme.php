<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Theme extends Model
{
    protected $table = 'theme';

    protected $hidden = [
        'id'
    ];

    /**
     * Feed global for all users
     * @return Builder
     */
    public static function getGlobalFeed()
    {
        $builder = self::addSelect('theme.title')
                       ->addSelect('theme.description')
                       ->addSelect('theme.content')
                       ->addSelect('theme.created_at')
                       ->addSelect('theme.updated_at')
                       ->addSelect('user.first_name AS user_last_name')
                       ->addSelect('user.last_name AS user_firt_name')
                       ->addSelect('user.slug AS user_slug')
                       ->addSelect('category.title AS category_title')
                       ->addSelect([
                           DB::raw('sha1(theme.id) slug'),
                           DB::raw('(select group_concat(distinct t.name)
                                    from tag as t
                                    where t.theme_id = theme.id) tag'),
                           DB::raw('(select ifnull(round(avg(r.value)), 0)
                                    from rating as r
                                    where r.theme_id = theme.id
                                    and r.value between 0 and 5 ) rating')
                       ])
                       ->join('user', 'user.id', '=', 'theme.user_id')
                       ->join('category', 'category.id', '=', 'theme.cat_id')
                       ->where([
                           'theme.status' => '1',
                           'user.status' => '1',
                           'category.status' => '1'
                       ])
                       ->orderBy('theme.created_at', 'desc');

        return $builder;
    }
}
