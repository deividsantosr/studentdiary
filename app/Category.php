<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Category extends Model
{
    protected $table = 'category';

    protected $hidden = [
        'description',
        'status',
        'created_at',
        'updated_at'
    ];

    /**
     * Get list category basic
     * @return Collection
     */
    public static function getSimpleList()
    {
        $collection = self::where('status', 1)
                          ->orderBy('title', 'asc')
                          ->get();

        return $collection;
    }
}
