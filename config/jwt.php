<?php

return [
    'iss' => env('JWT_ISS', 'forge'),

    'aud' => env('JWT_AUD', 'forge'),

    'exp' => env('JWT_EXP', false),

    'key' => env('JWT_KEY', 'forge')
];