<?php

return [
    'value_1' => env('POINT_VALUE_1', 0),

    'value_2' => env('POINT_VALUE_2', 0),

    'value_3' => env('POINT_VALUE_3', 0),

    'value_4' => env('POINT_VALUE_4', 0),

    'value_5' => env('POINT_VALUE_5', 0)
];